<?php
/*
	Plugin Name: Assessment 1
	Description: Assessment 1 Plugins
	Author: Dimas Mahendra Kusuma
*/
	/*
		Nomor 1
		Functipn form_testimonial berfungsi untuk menampilkan inputan form di halaman depan.
	*/
	function form_testimonial() { ?>
		<form action="<?php echo site_url() ;?>" method="get">
			<input name="sendername" placeholder="Name" type="text">
			<input name="senderemail" placeholder="Email" type="text">
			<textarea name="message" placeholder="Testimonial" rows="8"></textarea>
			<button type="submit">Submit</button>
        </form>
	<?php }
	add_shortcode( 'shortcode_form_testimonial', 'form_testimonial' );
	/*======================== Batas nomor 1 =====================*/

	/*
		Nomor 2
		deliver_testimonial : erfungsi untuk menyimpan inputan ke dalam databases dengan table wp_testimonial
	*/
	function deliver_testimonial() {
	    global $wpdb;	    
	    if (isset($_GET['sendername']) && isset($_GET['senderemail']) && isset($_GET['message'])) {
		    $insert = array(
				'name' => $_GET['sendername'], 
				'email' => $_GET['senderemail'],
				'testimonial' => $_GET['message']
			);
			$type = array( '%s', '%s', '%s' );
	        if ( $wpdb->insert('wp_testimonial', $insert, $type) ) 
	        {
	            echo '<div>';
	            echo '<p>Thanks for filling the testimonial.</p>';
	            echo '</div>';
	        } 
	        else 
	        {
	            echo 'An unexpected error occurred';
	        }
	    }
	}
	add_action("init", "deliver_testimonial");
	/*============================================= Batas nomor 2 ================================================*/
	
	/*
		Nomor 3
		Menampilkan menu testimonial di halaman admin.
	*/
	add_action( 'admin_menu', 'my_admin_menu' );

	function my_admin_menu() {
		add_menu_page( 'Testimonials', 'Testimonials', 'manage_options', 'myplugin/myplugin-admin-page.php', 'myplguin_admin_page', 'dashicons-tickets', 6  );
	}

	function myplguin_admin_page(){
		global $wpdb;
		$testi = $wpdb->get_results( "SELECT * FROM wp_testimonial" , ARRAY_A);
	    ?>
	        <table id="tabelTesti">
	            <thead>
					<tr>
						<th>No</th>
						<th>Nama</th>  
						<th>Email</th>  
						<th width="30%">Testimonial</th>	
					</tr>
	            </thead>
	            <tbody>
					<?php
						$no = 1;            
						foreach ($testi as $row)
						{ ?>
						    <tr>
						        <td><?php echo $no;$no++; ?></td>
						        <td><?php echo $row['name']; ?></td>
						        <td><?php echo $row['email']; ?></td>	                            
						        <td><?php echo $row['testimonial']; ?></td>
						    </tr>
					<?php } ?>            
	            </tbody>              
	        </table>
	    </div>
	<?php 
	}
	/*============================================= Batas nomor 3 ================================================*/


	/*Nomor 4*/
	add_shortcode('halaman_testi', 'testimonial_page');

	function testimonial_page(){
		global $wpdb;
		$testi = $wpdb->get_results( "SELECT * FROM wp_testimonial", ARRAY_A);
	    ?>
	        <table id="tabelTesti">
	            <thead>
					<tr>
						<th>No</th>
						<th>Nama</th>  
						<th>Email</th>  
						<th>Testimonial</th>	
						<th>Keterangan</th>
					</tr>
	            </thead>
	            <tbody>
	                <?php
						$no = 1;            
						foreach ($testi as $row)
						{ ?>
							<tr align="center">
							    <td><?php echo $no;$no++; ?></td>
							    <td><?php echo $row['name']; ?></td>
							    <td><?php echo $row['email']; ?></td>	                            
							    <td><?php echo $row['testimonial']; ?></td>
							    <td><form action="" method="post">
							    		<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
										<button type="submit">Send Email</button>
							        </form>
							   	</td>
							</tr>
	                <?php } ?>            
	            </tbody>              
	        </table>
	    </div>
	<?php }
	/*============================================= Batas nomor 4 ================================================*/


	/*Nomor 5*/
	function send_testimonial()
	{
	    global $wpdb;
	    $id = isset($_POST['id']) ? $_POST['id'] : '';
	    if($id) {
	    	$testi = $wpdb->get_results( "SELECT * FROM wp_testimonial WHERE id = " . $id , ARRAY_A);
	    	$name = isset($testi[0]['name']) ? $testi[0]['name'] : '';
		    $email = isset($testi[0]['email']) ? $testi[0]['email'] : '';
		    $message = isset($testi[0]['testimonial']) ? $testi[0]['testimonial'] : '';

		    $to = $email;
		    $subject = 'Testimonial';
	        $msg = '<table>
	                    <tr>
	                        <td>Name</td>
	                        <td>' . $name . '</td>
	                    </tr>                    
	                    <tr>
	                        <td>Message</td>
	                        <td>' . $message . '</td>
	                    </tr>
	                </table>';
	        wp_mail($to, $subject, $msg);
		    wp_die();

	    }
	}
	add_action('init', 'send_testimonial');
	/*============================================= Batas nomor 5 ================================================*/

?>